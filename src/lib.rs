use base64::{STANDARD, Config, DecodeError};
use std::{
    result,
    fmt::{self, Formatter, Debug, Display},
};

const CONFIG: Config = STANDARD;

enum HeaderByte {
    Coin = 0x01,
    Item = 0x02,
    Npc = 0x03,
    MapPoi = 0x04,
    PvPGame = 0x05,
    Skill = 0x06,
    Trait = 0x07,
    User = 0x08,
    Recipe = 0x09,
    Wardrope = 0x0A,
    Outfit = 0x0B,
    WvWObjective = 0x0C,
    BuildTemplate = 0x0D,
}

pub enum Error {
    InvalidChatLink,
    InvalidHeader,
    DecodeError(DecodeError),
}

impl Debug for Error {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        use Error::*;
        match self {
            InvalidChatLink => write!(f, "invalid chatlink"),
            InvalidHeader => write!(f, "invalid header"),
            DecodeError(err) => write!(f, "{:?}", err),
        }
    }
}

type Result<T> = result::Result<T, Error>;

fn remove_enclosing(s: &str) -> Option<&str> {
    s.strip_prefix("[&")?.strip_suffix(']')
}

/// Data contained in an itemlink.
#[derive(Clone, Debug, Default, PartialEq, Eq)]
pub struct Item {
    pub quantity: u8,
    pub id: u32,
    pub upgrades: [u32; 2],
    pub skin: u32,
}

impl Item {
    /// Decode an item from its chatlink.
    pub fn decode(s: &str) -> Result<Self> {
        // Unwrap the chatlink.
        let s = match remove_enclosing(s) {
            Some(s) => s,
            None => return Err(Error::InvalidChatLink),
        };

        let buf = base64::decode(s).unwrap();

        if buf.len() < 5 || buf[0] != HeaderByte::Item as u8 {
            return Err(Error::InvalidHeader);
        }

        let mut item = Self::default();

        let mut flag_byte = 0;
        for (i, b) in buf.iter().enumerate() {
            match i {
                // Byte 0 defines the quanity.
                1 => item.quantity = *b,
                // Bytes 2-4 define the item id, encoded as 24-bit little-endian.
                2 | 3 | 4 => item.id += (*b as u32) << (8 * (i - 2)),
                // Byte 5 defines the item flags, which define the rest of the link.
                // Is flag_byte 0 then it acts as the terminator byte.
                5 => flag_byte = *b,
                // If flag_byte & 0x80, bytes 6-8 define the wardrope skin, otherwise
                // they define the first upgrade.
                6 | 7 | 8 => match (flag_byte & 0x80) != 0 {
                    true => item.skin += (*b as u32) << (8 * (i - 6)),
                    false => item.upgrades[0] += (*b as u32) << (8 * (i - 6)),
                },
                // If the previous bytes defined the skin then bytes 10-12 define
                // the first upgrade, otherwise the second upgrade.
                10 | 11 | 12 => match (flag_byte & 0x80) != 0 {
                    true => item.upgrades[0] += (*b as u32) << (8 * (i - 10)),
                    false => item.upgrades[1] += (*b as u32) << (8 * (i - 10)),
                },
                // Bytes 14-16 define the second upgrade.
                14 | 15 | 16 => item.upgrades[1] += (*b as u32) << (8 * (i - 14)),
                _ => (),
            }
        }

        Ok(item)
    }
}

#[cfg(test)]
mod tests {
    use super::Item;

    #[test]
    fn test_item_decode() {
        let link = "[&AgH1WQAA]";
        let item = Item::decode(link).unwrap();
        assert_eq!(item, Item {
            quantity: 1,
            id: 23029,
            upgrades: [0, 0],
            skin: 0,
        });

        // Weapon only
        let link = "[&AgGqtgAA]";
        let item = Item::decode(link).unwrap();
        assert_eq!(item, Item {
            quantity: 1,
            id: 46762,
            upgrades: [0, 0],
            skin: 0,
        });

        // Weapon with Sigil 1
        let link = "[&AgGqtgBA/18AAA==]";
        let item = Item::decode(link).unwrap();
        assert_eq!(item, Item {
            quantity: 1,
            id: 46762,
            upgrades: [24575, 0],
            skin: 0,
        });

        // Weapon with Sigil 1 and Sigil 2
        let link = "[&AgGqtgBg/18AACdgAAA=]";
        let item = Item::decode(link).unwrap();
        assert_eq!(item, Item {
            quantity: 1,
            id: 46762,
            upgrades: [24575, 24615],
            skin: 0,
        });

        // Weapon with Wardrobe Skin
        let link = "[&AgGqtgCAfQ4AAA==]";
        let item = Item::decode(link).unwrap();
        assert_eq!(item, Item {
            quantity: 1,
            id: 46762,
            upgrades: [0, 0],
            skin: 3709,
        });

        // Weapon with Wardrobe Skin and Sigil 1
        let link = "[&AgGqtgDAfQ4AAP9fAAA=]";
        let item = Item::decode(link).unwrap();
        assert_eq!(item, Item {
            quantity: 1,
            id: 46762,
            upgrades: [24575, 0],
            skin: 3709,
        });

        // Weapon with Wardrobe Skin, Sigil 1 and Sigil 2
        let link = "[&AgGqtgDgfQ4AAP9fAAAnYAAA]";
        let item = Item::decode(link).unwrap();
        assert_eq!(item, Item {
            quantity: 1,
            id: 46762,
            upgrades: [24575, 24615],
            skin: 3709
        });
    }
}
